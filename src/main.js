import DummyButton from './components/DummyButton.vue'

const ComponentLibrary = {
  install(Vue) {
    Vue.component(DummyButton.name, DummyButton)
  }
}

export default DummyButton;

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(ComponentLibrary)
}